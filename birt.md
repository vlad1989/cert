**Privacy Policy**

web.master69 built the Speed ​​Bird Game (com.speedbird.game) app as free app. This SERVICE is provided by web.master69 at no cost and is intended for use as is.

The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at Speed ​​Bird Game (com.speedbird.game) unless otherwise defined in this Privacy Policy.

**Information Collection and Use**

For a better experience, while using our Service, We may require you to provide us with certain personally identifiable information. The information that We request will be retained on your device and is not collected by us in any way.

Link to privacy policy of third party service providers used by the app

*   [Google Play Services](https://www.google.com/policies/privacy/)

**Service Providers**

We may employ third-party companies and individuals due to the following reasons:

*   To facilitate our Service;
*   To provide the Service on our behalf;
*   To perform Service-related services; or
*   To assist us in analyzing how our Service is used.

**Links to Other Sites**

This Service may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by us. Therefore, We strongly advise you to review the Privacy Policy of these websites. We have no control over and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.

**Changes to This Privacy Policy**

We may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. We will notify you of any changes by posting the new Privacy Policy on this page.

This policy is effective as of 2020-08-01

**Contact Us**

If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us at web.master69@yandex.ru.